# Astrovel-Trainee-Challenge



## Author

Sergio Fernandez

***
## Description

Finalice con los objetivos basicos planteados, se agrego un versionado a la Api, entradas para las respectivas entidades.

La carga de datos se sigue realizando por medio de un script al inicio del servidor, por lo que tarda un par de minutos en finalizarse, la misma es en memoria, por lo que reduje el tamaño de la muestra.

Implemente un pequeño script para cargar una base de datos fisicas, pero esto quitaria un poco de portabilidad por lo que se mantuvo la persistencia en memoria.

Ya está creado el archivo dockerfile, con lo que es posible crear una imagen, para luego emplear un contenedor.

## Branch

```
git checkout master
```

## Build

```
gradlew build
```
## Build without tests, faster

```
gradlew build -x test
```
## Docker

```
docker build -t desafio-practicante-astrovel .

docker run -p 8080:8080 desafio-practicante-astrovel
```

## Status

Seguire puliendo detalles, para optimizar el rendimiento de la API, fue una experiencia bastante enriquecedora aprendi bastante de Kotlin durante esta ultima semana, y me enfrente a desafios interesantes (devolver un JSON, siendo algo tan sencillo en JavaScript, y es toda una travesia en el framework de Spring, o la division y manejo de indices de tipo Long).

## TO DO:

* Documentacion, testeo con Postman y subir el mismo (disponible de manera local)
* Script para creacion de base de datos en MySql.
* Despliegue, los servidores seleccionados no fueron los apropiados, por lo que se desperdicio tiempo en los mismos.
* Agregar mas atributos relevantes para las entidades que representan la logica del negocio.